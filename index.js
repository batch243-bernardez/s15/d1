// console.log("Hello World");

// [Section] Syntax, Statements, and Comments (Single line and Multi-line)

// Statements in programming are instructions that we tell the computer to perform
// JS Statements usually end with semicolon (;) (delimiters)

// Semicolons are not required in JS but we will use it to help us train to locate where a statements ends.

// A syntax in programming is a set of rules that describes how statements must be constructed (formula that needs to be followed)

// All lines or blocks of code should be written in specific manner to work. This is due to how these codes where initially program to function in certain manner.

// Comments - parts of the code that gets ignored by the language
// Comments are meant to describe the written code
	// This is the single line comment (two slashes)
	/*This is the multi-line comment (slash and asterisk)*/


// [Section] Variables

// Variables are used to conatin data.
// Any information that is used by an application is stored in the memory (of units)
// When we create variables, cartain portions of a device's memory is given a name that we call variables

// This makes it easier for us to associate information stored in our devices to actual "names" about information

// Declaring variables - it tells our devices that a variable name is created is created and is ready to store data.
// Declaring a varibale without giving it a value will automatically assign it with a value of "undefined", meaning is that the variable's value was not defined.

	// Syntax
		// let / const variableName;


let myVariable = "Ada Lovelace";
// const myVariable = "Ada Lovelace";

// console.log() - useful for printing values of variables or certain results of codes into the google chrome browser's console
// Constant use of this, throughout developing an application will save us time and builds good habit in always checking for the output of our code.

console.log(myVariable);

/*
	Guides in Writing Variables:
	1. Use the let keyword followed by the variable name of your choice and use the assignment operator (=) to assign value.
	2. Variable names should start with lowercase character, use camelCase for multiple words.
	3. Constant variables (const), use the 'const' keyword.
		Using 'let': we can change the value of the variable (can be reassign)
		Using 'const': we cannot change the value of the variable
	4. Variable names should be indicative (descriptive) of the value being stored to avoid confusion.
*/

// Declare and initialize variables
	// Initializing variables - the instance when a varibale is given it's initial starting value (initialized value)
		// Syntax
			// let/const variableName = value; (examples below)

			let productName = "desktop computer";
			console.log (productName)

			let productPrice = "18999"
			console.log (productPrice)

//In the context of certain applications, some variables/information are constant and should not be changed. 
	// In this example. the interest rate for a loan, saving account or a mortgeg must not be changed due to real world concerns.

	const interest = 3.539;
	console.log (interest);

// Reassigning variable values
// Reassigning a variable means changing its initial or previous value into another value
	// Syntax
		// variableName = newValue;

productName = "laptop";
console.log (productName);
console.log (productName);

/*let productName = "laptop";
console.log(productName);*/

		// let variable cannot be re-declared within its scope.


// Values of constants cannot be changed and will simply return an error.
// interest = 4.489;
// console.log (interest);


// Reassigning variables and initializing variables
let supplier; 
	// initialization
supplier = "Zuitt store"
	// reassignment
supplier = "Zuitt merch"

const name = "George Alfred Cabaccang";
/*name = "George Alfred Cabaccang";
cannot be reasign*/


// var vs let/const
	// some of you may wonder why we used let and const keyword in declaring a variable when we search online, we usually see 'var' keyword.
	// var - also used in declaring variable. but var is an EcmaScrpt1 feature [ES1 (Javasript 1997)]
	// let/const was introduced as a new feature in ES6 (2016)

// hoisted
a=5;
console.log(a);
var a;

/*b=6
console.log(b);
let b;*/


// let/const are local/global scope
// scope - where these variables are available for use
// let and const are block scope (dun lang pwede gamitin)
	// block - is a chunk of code bounded by {}. a block in curly braces. anything within the curly braces is a block.
// globally - kapag inaccess ang varibale ulit, kahit malayo, pwede pa rin sya magamit

let outerVariable = "hello from the other side";
	/*{
		let innerVariable = "hello from the block";
		console.log(innerVariable);
		console.log(outerVariable);
	}*/

	{
		let innerVariable = "hello fron the second block";
		console.log(innerVariable);
		let outerVariable = "hello from the second block";
	}

console.log(outerVariable);
// console.log(innerVariable);


// Multiple variable declaration
// Multiple variables can be declared in one line

let productCode = "DC017", productBrand = "Dell";
console.log(productCode, productBrand);
console.log(productCode);
console.log(productBrand);

// using a variable with a reserved keyword
/*const let = "hello";
console.log(let);*/



// [Sections] Data Types
// Strings - series of characters that creates a word, phrase, a sentence, or anything related to creating text
// Strings in Javascript - can be written using either single (') and doube (")
// In other programming languages, only the double can be used  for creating strings.

let country = "Philippines";
let province = 'Metro Manila';

// Concatenate
// Multiple string values can be combined to create a single string using the "+" symbol.

let fullAddress = province + ", " +country;
console.log(fullAddress);

let greeting = 'I live in the ' + country;
console.log(greeting);


// The escape characters (\) in strings in combination with other characters can produce different effects.

// "\n" - refers to creating a new line between text (break)
let mailAddress = "Metro Manila\nPhilippibes";
console.log(mailAddress);

let message = "John's employees went home early";
message = 'John\'s employees went home early';
console.log(message);


// Numbers
// Intergers (whole numbers)l
let count = "26";
let headCount = 26;
console.log(count);
console.log(headCount);

// Decimal Numbers or Fractions
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

console.log("John's grade last quarter is " + grade)
console.log(count + headCount);


// Boolean
// Boolean values are normally used to store values relating to the state of certain things
// This will be useful in further discussions about creating logic to make our application respond to certain scenarios

let isMarried = false;
let inGoodConduct = true;
console.log(isMarried);
console.log(inGoodConduct);

console.log("isMarried: " + isMarried);


// Arrays
// Arrys are a special kind of data type that's used to store multiple values. 
// Arrays can store different data types but is normally used to store similar data types

	// Similar data types
		// Syntaxt
		// let/const arrayName = [element!, elementB, elementC ...]
		// index:     0      1     2     3
		
		let grades = [98.7, 92.1, 90.2, 94.6];
		console.log(grades[1]);
		console.log(grades);


//Different Data Types
// Storing different data types inside an arra is not recommended because it will not make sense in the context of programming.

let details = ["John", "Smith", 32, true];
console.log(details);


// Objects
// Objects are another special kind of data type that is used to mimic real qorld objects or items
// They are also used to create a complex data that contains pieces of information that are relevant to each other.
	// Syntax: 
	/*let/const objectName = {
		propertyA: value,
		propertyB: value,
	}*/

	let person = {
		fullName: "Juan Dela Cruz",
		age: 35,
		isMarried: false,
		contact: ["0916345678", "1234 5678"],
		address: {
			houseNumber: '345',
			city: "Manila"
		}
	}
	console.log(person);
	console.log(person.age);
	console.log(person.contact[1]);
	console.log(person.address.city);


// typeof operator is used to determine the type of data or value of a variable 

console.log(typeof person);
// Note: Array is a special type of object with methods and functions to manipulate it. (We will discuss these method in S22 Javascript array manipulation)
console.log(typeof details);


// Constant Objects and Arrays
	/*The keyword const is a little misleading.
	It does not define a constant value. It defines a constant reference to a value.

	Because of this you can NOT:
	Reassign a constant value
	Reassign a constant array
	Reassign a constant object

	But you CAN:
	Change the elements of constant array
	Change the properties of constant object*/

const anime = ['one piece', 'one punch man', 'attack on titans'];
/*anime = ['kimetsu no yaiba']*/
console.log (anime);
anime[0] = 'kimetsu no yaiba';
console.log (anime);
anime[5] = 'dxd';
console.log (anime);
console.log (anime[4]);


// Null
// It is used to intentionally expressed the absence of a value in a variable/initialization
// Null simply means that a data type was assigned to a variable but it does not hold any value or amount or is nullified
let spouse = null;
console.log(spouse);


// Undefined
// It represents the state of a variable that has been declared but without value
let fullName;
console.log(fullName);

// Undefined vs Null
// Undefined - variable was created buut not provided a value
// Null - variable was created and was assigned a value that does not hold any value or amount









































































